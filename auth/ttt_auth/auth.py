from django.http import JsonResponse
import json
import jwt
import re
import game.log.logging_api as logger

SECRET = "jjfkecmwKJMd_effje2rfk394rf_KFMdsckW34x_ckr"
USER_DB = {}

emailRegex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'



def login(request):
    if request.method != "POST":
            logger.write_lo_log('auth:bad request method','ERROR')
            return JsonResponse({})
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    email = body['email']
    if email not in USER_DB:
        # Already singup
        logger.write_lo_log('auth:user does not exist', 'ERROR')
        return JsonResponse({'err':'USER NOT EXISTS'}, safe=False)
    jwtToken = jwt.encode({'email': email}, SECRET, algorithm='HS256')
    logger.write_lo_log('auth:successful login', 'INFO')
    return JsonResponse({'jwt':jwtToken.decode("utf-8") }, safe=False)

def signup(request):
    if request.method != "POST":
            logger.write_lo_log('auth:bad request method','ERROR')
            return JsonResponse({})
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    email = body['email']
    if email in USER_DB:
        # Already singup
        logger.write_lo_log('auth:user already exists', 'ERROR')
        return JsonResponse({'err':'USER ALREADY EXISTS'}, safe=False)
    # validate that email is valid address
    if not re.search(emailRegex,email):
        logger.write_lo_log('auth:email not valid', 'ERROR')
        return JsonResponse({'err':'NOT A VALID EMAIL'}, safe=False)
    USER_DB[email] = True
    jwtToken = jwt.encode({'email': email}, SECRET, algorithm='HS256')
    logger.write_lo_log('auth:successful signup', 'INFO')
    return JsonResponse({'jwt':jwtToken.decode("utf-8") }, safe=False)